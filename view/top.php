<!DOCTYPE html>
<html>
<head>
	<title>Mandalay Directory</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
	<link rel="stylesheet" type="text/css" href="assets/css/w3.css">
</head>
<body class="bb">
	


	<nav class="navbar fixed-top container-fluid  navbar-toggleable-md navbar-light bb">
		
	  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <a class="navbar-brand english" href="paging.php">HOME</a>
	
	
	 <div class="collapse navbar-collapse ml-auto" id="navbarSupportedContent">
	    <form action="search.php" method="post" class="form-inline ml-auto my-lg-0">
	      <input class="form-control mr-sm-2" list="CompanyType" placeholder="Enter Company Type" name="CompanyType">
	      <datalist id="CompanyType">
	      	<option value="Cosmetics">Cosmetics</option>
	      	<option value="Electronics">Electronics</option>
	      	<option value="Constructions">Constructions</option>
	      	<option value="Travels&Tours">Travels&Tours</option>
	      	<option value="Store">Store</option>
	      	<option value="Airways">Airways</option>
	      	<option value="Sports">Sports</option>
	      	<option value="Foods&Drinks">Foods&Drinks</option>
	      	<option value="Trading">Trading</option>
	      	<option value="Adversiting">Adversiting</option>
	      </datalist>
	      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="submit" name="submit">Search</button>
	    </form>
 	 </div>
	</nav>
	

	

	<div class="container-fluid">
		<header class="jumbotron-fluid jumbotron m-0">
			<h1 class="english text-white display-0.5 text-center">
				Mandalay Business Directory
			</h1>
		</header>
	</div>

-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2018 at 11:22 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `charli`
--

-- --------------------------------------------------------

--
-- Table structure for table `tablejoin`
--

CREATE TABLE `tablejoin` (
  `NO` int(10) NOT NULL,
  `TypeId` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tablejoin`
--

INSERT INTO `tablejoin` (`NO`, `TypeId`) VALUES
(1, 4),
(2, 9),
(3, 4),
(4, 7),
(5, 3),
(6, 9),
(7, 4),
(8, 2),
(9, 5),
(10, 10),
(11, 2),
(12, 2),
(13, 8),
(14, 2),
(15, 1),
(16, 6),
(17, 2),
(18, 6),
(19, 2),
(20, 8),
(21, 10),
(22, 5),
(23, 1),
(24, 9),
(25, 4),
(26, 10),
(27, 10),
(28, 7),
(29, 4),
(30, 9),
(31, 6),
(32, 1),
(33, 5),
(34, 6),
(35, 7),
(36, 10),
(37, 10),
(38, 8),
(39, 5),
(40, 7),
(41, 1),
(42, 9),
(43, 7),
(44, 2),
(45, 7),
(46, 5),
(47, 3),
(48, 7),
(49, 3),
(50, 6),
(51, 1),
(52, 8),
(53, 3),
(54, 9),
(55, 6),
(56, 1),
(57, 5),
(58, 10),
(59, 8),
(60, 8),
(61, 9),
(62, 8),
(63, 3),
(64, 1),
(65, 4),
(66, 3),
(67, 2),
(68, 9),
(69, 4),
(70, 6),
(71, 1),
(72, 2),
(73, 5),
(74, 5),
(75, 10),
(76, 10),
(77, 1),
(78, 7),
(79, 3),
(80, 4),
(81, 2),
(82, 6),
(83, 1),
(84, 4),
(85, 9),
(86, 7),
(87, 6),
(88, 6),
(89, 5),
(90, 9),
(91, 3),
(92, 4),
(93, 5),
(94, 8),
(95, 3),
(96, 7),
(97, 10),
(98, 8),
(99, 3),
(100, 8);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

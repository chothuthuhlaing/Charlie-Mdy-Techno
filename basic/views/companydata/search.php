<?php 
	use yii\helpers\Html;
    use yii\grid\GridView;
    //echo $this->render('_search', ['model' => $searchModel]);
 ?>
  <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NO',
            'CompanyName',
            'Address',
            'PhoneNumber',
            'Email:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
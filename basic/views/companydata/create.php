<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Companydata */

$this->title = 'Create Companydata';
$this->params['breadcrumbs'][] = ['label' => 'Companydatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="companydata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanydataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companydatas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="companydata-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Companydata', ['create'], ['class' => 'btn btn-success']) ?>
        <?php
            $form = ActiveForm::begin([
            'action' => ['search'],
            'method' => 'get',
             ]);
        ?>
        <div class='form-inline my-2 my-lg-0'>
            <?= "<input class='form-control mr-sm-2' type='text' name='search' placeholder='Enter Company Name'>" ?>
            <?= "<button class='btn btn-primary btn-outline-success my-2 my-sm-0' type='submit'>Search</button>" ?>
        </div>
        <?php ActiveForm::end(); ?>
        
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NO',
            'CompanyName',
            'Address',
            'PhoneNumber',
            'Email:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

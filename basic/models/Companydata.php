<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "companydata".
 *
 * @property int $NO
 * @property string $CompanyName
 * @property string $Address
 * @property string $PhoneNumber
 * @property string $Email
 */
class Companydata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'companydata';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CompanyName', 'Address', 'PhoneNumber', 'Email'], 'required'],
            [['CompanyName', 'Address', 'PhoneNumber', 'Email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NO' => 'No',
            'CompanyName' => 'Company Name',
            'Address' => 'Address',
            'PhoneNumber' => 'Phone Number',
            'Email' => 'Email',
        ];
    }
}
